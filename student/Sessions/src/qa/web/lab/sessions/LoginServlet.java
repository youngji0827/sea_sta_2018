package qa.web.lab.sessions;

import java.io.IOException;
import java.text.DecimalFormat;

import javax.servlet.ServletException;
import javax.servlet.http.*;

/**
 * @version 	1.0
 * @author lewis
 */
public class LoginServlet extends HttpServlet {

	private static final DecimalFormat nf = new DecimalFormat("00000");

	/**
	* @see javax.servlet.http.HttpServlet#void (javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	*/
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {

		/*
		 * QA To do: Exercise 2 step 3
		 * 
		 * Obtain the values of the form parameters, and use the 
		 * private method checkLogin() to see if they match. Notice 
		 * that the password is a number which depends on the username 
		 * - users don�t get to choose their own password, they get 
		 * told when they register. If the user fails to log in 
		 * successfully, you should redirect back to the login page. 
		 * If their password is correct, store the username as an 
		 * attribute of the session, and redirect to the �loggedin� page.
		 */

	}

	/**
	* @see javax.servlet.http.HttpServlet#void (javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	*/
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
			doGet(req, resp);
	}

	private boolean checkLogin(String username, String password) {
		if (username == null || password == null) return false;
		int hash = Math.abs(username.hashCode() % 100000);
		String hashString = nf.format(hash);
		System.out.println("Password ought to be: " + hashString);
		return password.equals(hashString);
	}

}
