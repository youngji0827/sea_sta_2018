package qa.shop;

import java.util.*;

public class Cart {

	private Map items = new HashMap();
	
	public void addItem(ShopItem item) {
		if (items.containsKey(item)) {
			Integer numberInteger = items.get(item);
			int number = numberInteger.intValue();
			number++;
			items.put(item, new Integer(number));
		} else {
			items.put(item, new Integer(1));
		}
	}
	
	public void clear() {
		items.clear();
	}
	
	/**
	 * Two for the price of one!!
	 * @return the total price of the items in the cart 
	 */
	public double getPrice() {
		double price = 0.0;
		
		Iterator iter = items.keySet().iterator();
		while (iter.hasNext()) {
			ShopItem item = (ShopItem) iter.next();
			Integer numberInteger = items.get(item);
			int number = numberInteger.intValue();
			price += ((number + 1) / 2) * item.getPrice();
		}
		
		return price;
	}

}
