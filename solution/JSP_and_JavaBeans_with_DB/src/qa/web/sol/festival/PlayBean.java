package qa.web.sol.festival;

import java.io.Serializable;
import javax.sql.DataSource;
import java.sql.*;
import javax.naming.*;

/**
 * @author Lewis
 *
 */
public class PlayBean implements Serializable {
	private DataSource ds;
	
	private String play;
	private String author;
	private String time;
	private int day;
	private boolean resultsObtained;
	
	/**
	 * Constructor for PlayBean.
	 */
	public PlayBean() {
		try {
			Context ic = new InitialContext();
			ds = (DataSource) ic.lookup("java:comp/env/jdbc/festival");
			ic.close();			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * The setDay() method with the DB access code.
	 */
	public void setDay(int day) {		
		this.day = day;
		Connection con = null;
		try {
			con = ds.getConnection();
			Statement stmt = con.createStatement();
			String sql = "SELECT * FROM DBO.plays WHERE festivalday=" + day;
			System.out.println("Executing: " + sql);
			ResultSet res = stmt.executeQuery(sql);
			if (res.next()) {
				resultsObtained = true;
				play = res.getString(1);
				author = res.getString(2);
				time = res.getString(3);
			} else {
				resultsObtained = false;
				play = null;
				author = null;
				time = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public boolean getResultsObtained() {return resultsObtained;}
	public String getPlay() {return play;}
	public String getAuthor() {return author;}
	public String getTime() {return time;}
	public int getDay() {return day;}
	
	
	

}
