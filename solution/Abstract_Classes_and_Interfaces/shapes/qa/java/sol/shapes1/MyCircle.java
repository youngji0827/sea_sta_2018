package qa.java.sol.shapes1;

import java.awt.*;

/*
 *
 * MyCircle
 *
 */

public class MyCircle extends MyShape
{
	//
	// Constructor
	//
	public MyCircle(int centreX, int centreY, int radius)
	{
		//
		// Convert the values to those held by
		// the superclass (i.e. left, top, width, height)
		//
		super(centreX - radius,
			  centreY - radius,
			  radius * 2,
			  radius * 2);
	}


	//
	// Must implement draw method defined in MyShape
	//
	public void draw(Graphics g)
	{
		//
		// Draw our circle
		//
		g.drawOval(left, top, width, height);
	}
}
